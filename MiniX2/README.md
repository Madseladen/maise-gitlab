# **MiniX2**

Her er et screenshot af mit program. Jeg ville gerne have haft en video, men det kunne jeg ikke lige finde ud af, hvordan jeg skulle gøre. 
<img src="Emojigrøn.png">
<img src="Emojiblå.png">

Her er et link til mit program: 
[https://maisealling.gitlab.io/maise-gitlab/MiniX2/index.html] 

Link til min code [https://gitlab.com/MaiseAlling/maise-gitlab/-/blob/main/MiniX2/Minix2.js]

Øjnene er blevet lidt underlige, når man trykker ind på linket, jeg ved ikke lige hvorfor, tror måske at det har noget med skærmbredten at gøre. 

## Hvad går mit program ud på
Mit program går ud på at jeg har lavet en emoji med to forskellige ansigterog to forskellige farver og på den måde lavet to emojis. Den første emoji smiler og er glad, samt er den gørn, hvor den anden emoji skal virke overrasket over noget, og er blå. Når man åbner mit program vil man se den første emoji, som smiler. Tryker man på musen og holder den nede vil den anden emoji komme frem på skærmen. Når man slipper musen igen vil den første emoji komme frem igen. Sådan kan man bliver ved. 
Min kode er som sådan meget simpel, men det føler jeg også emoji's skal være. Jeg har valgt at fokusere på mouseIsPressed funktionen, da vi kiggede på den i timen. 

## Mine tanker om emoji's og hvorfor jeg har lavet min kode sådan
Jeg bruger ikke selv emoji's så meget, hvis jeg bruger dem benytter jeg primært hjertet eller den almindelige glade smiley. 
Jeg tror aldrig jeg har skiftet farven på en emoji for at den skulle repræsentere mig. Jeg har svært ved at se grunden til at der skal være så mange forskellige emoji's med mad, sport, huse osv. jeg kan ikke rigtig se, hvad man skal bruge dem til. 
Jeg kan godt sætte mig ind i, hvorfor emoji's er blevet lavet om og at der er så mange forskellige, jeg forstå godt at nogle bliver krænket over det. Jeg tror måske bare ikke at det fylder så meget hos mig. Jeg ville være lidt ligeglad med om der var en minstruations emoji, men jeg kan godt se, hvorfor folk vil have det. Det er som om at de bruger emoji's til at lave et politisk statement. Nogle gange kan jeg nok bare blive lidt træt, fordi hvis man skal lave en emoji til alt og alt skal have en større betydning og et politisk statement, så kan man blive ved. Så kan man hele tiden sige whatabout... 
Jeg har derfor valgt at lave mine emoji's i to forskellige farver, som ikke kan associeres til nogle husfarver, med sorte øjne og en rød mund. Mine emoji's skal hverkan have arme, næse, øre eller ben. Man skal ikke kunne identificere sig selv i dem. Jeg har lavet den med kun to udtryk, et glad og et overrasket, fordi at jeg føler at en emoji er noget der skal repræsentere glæde og kærlighed til andre, samt lidt sjov. Jeg tænkte derfor at disse to nogenlunde neutrale emoji's var et godt alternativ. Mine emoji's taler måske ikke ind i debatten om at alle skal være repræsenteret, men mere ind i debatten om at en emoji måske bare er en emoji. 

## Min kode
Først satte jeg størrelsen på mit canvas og baggrundsfarven. 
~~~~
function setup() {
createCanvas(windowWidth, windowHeight); 
background(0, 0, 255)
~~~~
Så tegnede jeg min første smiley, til dette brugte jeg funktionen ellipse til formen og fill til farve. For at lave munden benyttede jeg funktionen arc(). Denne giver en form for halvcirkel. 
~~~~
//Hoved
fill(51, 204, 0); 
ellipse(windowWidth/2, windowHeight/2,400);
//ene hvide øje
fill(255, 255, 255);
ellipse(windowWidth/2.1, windowHeight/2.3, 100);
//andet hvide øje
fill(255, 255, 255);
ellipse(windowWidth/1.9, windowHeight/2.3, 100);
// ene sorte øje
fill(0);
ellipse(windowWidth/2.1, windowHeight/2.3, 50);
// ene sorte øje
fill(0);
ellipse(windowWidth/1.9, windowHeight/2.3, 50);
// Mund
fill(204, 51, 51);
arc(windowWidth/2, windowHeight/1.8, 150, 150, 0, PI); 
~~~~
Herefter ville jeg gerne have min emoji til at skifte ansigt når man klikkede på musen og holdte den inde. På den måde fik jeg lavet 2 emoji's. 
Til dette brugte jeg funktionen mouseIsPressed og if else statement. Jeg satte altså at hvis musen blev klikket og dette var stramt sandt, så skulle det nedenstående være på skærmen. Herefter tegnede jeg den anden emoji. 
~~~~

  if(mouseIsPressed === true) { // sætter at hvis musen bliver trykket, 
    //skal den skifte emoji, i den nedenstående
    fill(51, 204, 255); 
    ellipse(windowWidth/2, windowHeight/2,400);
      // det ene øje 
      fill(255, 255, 255);
      ellipse(windowWidth/2.1, windowHeight/2.3, 100);
      fill(0);
    ellipse(windowWidth/2.1, windowHeight/2.2, 50);
    //det andet øje
    fill(255, 255, 255);
ellipse(windowWidth/1.9, windowHeight/2.3, 100);
fill(0);
ellipse(windowWidth/1.9, windowHeight/2.2, 50);
fill(204, 51, 51); 
ellipse(windowWidth/2, windowHeight/1.8, 70, 70);
~~~~
Min kode er på en måde meget enkel, men jeg følte at jeg løste opgaven på et niveau som jeg kunne være med på. Jeg ville gerne have lavet min kode sådan, at når man trykkede på musen så skiftede man ansigt indtil man trykkede på musen igen. Det kunne jeg ikke lige finde ud af, så dette blev alternativet, hvilket jeg også synes er okay. 



