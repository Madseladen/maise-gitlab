## MiniX5

Dette er et link til mit program
https://maisealling.gitlab.io/maise-gitlab/Minix5/index.html

Dette er en gif af mit program 
<img src="MiniX5.gif">

## Kort beskrivelse af mit program
I mit program ser man en hvid baggrund med små firkanter i linjer, samt forskellig størrelse firkanter fordelt random på skærmen. Firkanterne og cirklerne skifter farver i nuancerne sort, grå og hvid. Når en bestemt nuance i intervallet 0 > 100 bliver ramt, så vil programmet stoppe og holde pause i 5 sekunder. Her efter vil det fortsætte som det slap. 

## Min kode
Til at starte med har jeg lavet globale variabler, som jeg kan bruge genenm min kode. I setup har jeg lavet canvas og background. 
~~~~
// Laver globale variabler
let distance; 
let size; 
let wait; 
let waitTime = 5000; 
let farve; 
~~~~
I starten af min function draw har jeg sat farve til at være randem i alle toner af grå. 
~~~~
function draw() {

//Sætter farven til random sort og hvid
    farve = random(255);
~~~~
Her laver jeg et for-loop, så min cirkler, vil bliver placeret rundt på canvaset random, og kør ei et loop (altså blive ved med dette)
~~~~
//Laver et for-loop, som gør at der kommer flere ellipse (50), i (index) sættes til 0, da den starter der fra og tæller op til 50, i ++, gør at der bliver lagt 1 til hele tiden. 
    for (let i=0; i < 50; i++){

//Denne gør at ellipsen palceres rundt på canvaset random 
    x = random(windowWidth);
    y = random(windowHeight);

// laver distancen mellem cirklerne og firkanterne, så den er større i den ene ende af canvaset og mindre i den anden. I den lille ende er den distancen/20. 
distance = dist(x, y, 100, y); 
size = distance/20;
noStroke();

//Vi skal tegne en ellipse
    ellipse(x, y, size);
    }
~~~~
Til slut laver jeg venter tiden i et if-statement. 
~~~~
// Laver et if-statement som siger at hvis farven er grå-ish, så vil den stoppe i ca. 5 sekunder
if(farve > 100) {
    // floor(millis()), betyder at wait skal være antallet af det millisekunder der gået siden programmet startede + vente tiden (5 sekunder)
wait = floor(millis()+ waitTime); 

// programmet stopper i 5 sekunder. 
while(millis() < wait) {}

}
~~~~

## Reglerne i mit generative program: 
Reglerne jeg har lavet i mit generative program er i mit for-loop og gør at den skal blive ved med at generere ellipser indtil den når 50 ellipser. 
Min anden regel er i mit if-statement, hvor jeg fortæller programmet at når farven er over 100 (farve-koordinater i grå-toner), så skal programmet holde pause og fryse skærmen i 5 sekunder eller 5000 millisekunder, fra programet er startet. 

## Beskrivelse af mit program, og hvordan det klare sig over tid, samt hvordan mine regler producere emergent adfærd: 
Når man først ser mit program, ligger man nok mærke til at nogle cirkler er større end andre og der er en sammenhæng i hvor de er placeret efter størrelse. Vi ser også at firkanterne er ens og er placeret på lige linjer. Dette er noget vi kan forholde os til at sætte i forbindelse med hinanden. Når mit program så begynder at køre, så køre det nogle gange hurtigt, hvor det skifter farve hurtigt, og andre gange så stopper det og holde pause længe, dette er random hvornår den gør det (ser det ud som). Men denne form for pause er blevet bestemt af nogle regelsæt, som jeg har placeret. For brugeren kan det derfor virke uforusigeligt, hvornår denne pause vil forkomme, men for mit program er det hele planlagt og tilrettelagt ned til mindste detalje. Mine regler spiller derfor en rolle i mit program, til at bestemme, hvornår den skal køre og hvornår den skal holde pause, samt hvor mange cirkler der skal blive placeret random på skærmen. 

Når man ser at programmet stopper første gang, vil man måske tro at den er gået i stå, at det er færdig med at køre eller at internettet er dårligt, og den lige skal loade. Når mit program begynder at køre hurtigt, tænker man måske at det nu køre som det skal. Men når det så begynder at køre hurtigt, stoppe, køre, stoppe, så kan man blive i tvivl og det er her denne følelse af uforusigelighed opstår. 

Men hvad er generativ kunst?
"Genreativ art refers to any practice where [sic] artist use a system, such as a set of natural languages, rule, a computer program, a machine, or other procedural invention, which is set into motion, with some degree of autonomy contrubuting to or resultong in a completed work of art." (Soon and Cox, 2020). 
Generativ kunst er altså kunst der bliver genereret af nogle regler som kunstneren sætter. Med generativ kunst, vil der også forekomme randomness. kunsteren vil altså opsætte nogle regler som programmet skal bevæge sig inden for, men det kan være random hvornår programmet præcist opfylde den regel eller befinder sig i det præcise interval som kunsneren har sat op. Kunsneren laver derfor den generative kunst uden nødvendigvis at kende til slutresultatet i forvejen. (http://da.artsentertainment.cc/kunst/Andet-kunst/1003002165.html). 

Da jeg lavede mit program, havde jeg nogle ideer omkirng hvordan det skule se ud, med de forskellige objekter og farver. Men eftersom at jeg har valgt random() funktionen, så bestemmer jeg som kunstner nødvendigvis ikke, hvordan mine objekter vil blive placeret, samt hvilek præcise farver der vil komme op hvornår, samt hvornår programmet vil stoppe. Jeg bestemmer det til en vis grad, da jeg har valgt farvenuancen, samt hvilket interval den kan køre  og stoppe i. Men det enedelige resultat har jeg inden kontrol over. 
Dette spiller godt overens med tanken om uforusigelighed. Vi har en ide om hvordan noget skal være, men hvordan det spiller sig ud og udvikler sig, har vi ikke nogen kontrol over. 

Jeg har bevidst valgt at bruge firkanter på linjer og cirkler placeret random. Firkanterne på linjerne, viser for mig denne ide om regler, og kasser, som man kan befinde sig i og udføre sit arbejde. I disse kasser ved du, hvad du skal, du ved hvad der vil ske og du er forberedt på det hele. Cirklerne, som er placeret random på skærmen, i random størrelser, er et modspil til dette. De repræsentere denne udvished, og kontrolløshed, som også forekommer i generativ kunst. Vi ved ikke hvordan programmet vil reagere på vores regler, eller hvordan det vil ende. 
Det at cirklerne også bevæger sig fra den ene side til den anden, og på en måde væk fra firkanterne, skal vise ar cirkler prøver at flygte fra firkanterne. De prøver ligesom at undvige de her regler, som prøver at holde dem fast. 

Man kan så sige at det er sjovt, at mine regler som jeg har opsat, mest omhandler cirklerne. Dette akn man dog argumentere for, at reglerne på en måde at til vil være der og samle kaoset og uforusigeligheden, så vi kan skabe en sammenhæng i de ting vi ser og nemmere forholde os til det. 

Regler kan altså være noget der samler og giver os en mening og et mål. Dette kan vi have brug for når noget uforusigeligt opstår. Samtidig så kan regler være irriterende og begrænsende, som kan gøre at man ikke føler at man kan udfolde sig kreativt. 

Denne opgave var meget splittet for mig. Jeg synes det var fedt at vi kunne være kreative og skulle lave noget form for kunst, men jeg blev irriteret over at jeg skulle have et for-loop eller et while-loop med, samt en conditional-statement. Jeg følte mig her meget begrænset og følte ikke jeg kunne udleve min kreativitet helt ud. 


