/* laver en variable video, 
så programmet ved at det skal bruge webcam*/
let video; 

// laver en variable for trackeren. 
let ctracker; 

/* laver en variable til knappen tilføj filter*/
let knap; 

// Laver en variable til knappen tilføj tøj
let knaptøj; 

// Laver en variable til knappen tilføj ansigt
let knapansigt; 

//Laver en global variable for positioner
let positioner; 

//LAver variabler til mit ansigt, tøj og filter
let mundnose; 
let brillerhat; 
let filter1; 


function setup() {

createCanvas(700, 700); // Den skal fylde siden ud??
/*Opretter kanppen med createButton, dette er et DOM-element*/
  knap = createButton("Add filter"); 

/*Placere knappen på canvas */
knap.position(300, 600); 

/*Sætter størrelsen på knappen */
knap.size(100, 50); 

// Gør sådan at når man trykker på knappen tilføj filter, så vil variablen filter1, være sand. 
knap.mousePressed(() => filter1 = true); 








/*Opretter kanppen knaptøj med createButton og sætter teksten til tilføj tøj*/
knaptøj = createButton("Add clothes"); 

/*Placere knappen på canvas */
knaptøj.position(0, 600); 

/*Sætter størrelsen på knappen */
knaptøj.size(100, 50); 

// Gør sådan at når man trykker på knappen tilføj tøj, så vil variablen brillerhat, være sand. 
knaptøj.mousePressed(() => brillerhat = true); 





/*Opretter kanppen knapansigt med createButton og sætter teksten til tilføj tø*/
knapansigt = createButton("Add face"); 

/*Placere knappen på canvas */
knapansigt.position(600, 600); 

/*Sætter størrelsen på knappen */
knapansigt.size(100, 50); 

// Gør sådan at når man trykker på knappen tilføj ansigt, så vil variablen mundnose, være sand. 
knapansigt.mousePressed(() => mundnose = true); 






  /*createCapture er funktionen som vil kreere 
  et capture objekt, som er i video, gør at jeg kan se mig selv*/
  video = createCapture(VIDEO); 

//Sætter størrelsen på videoen
  video.size(700, 700); 

  /* gør at vidoen er på den øverste del af 
  skærmen, ellers ville den have lavet to skærme*/
  video.hide(); 

 // tilføjer min trecker, tildeler variablen tracker, tracker elementet. 
ctracker = new clm.tracker(); 
ctracker.init(pModel); 
ctracker.start(video.elt); 
}






function draw() {

  /* benytter image, for at tage billedet som
  videoen skaber oppe i canvaset som jeg har lavet
  sætter det i koordinatet 0,0, da det skal starte her*/
  image(video, 0, 0, 700, 700); 

// Laver teksten som skal stå øverst på skærmen
  push(); 
  textSize(30); 
  fill(51, 255, 0); 
  text('Press the buttons and hide your identity', 100, 100)
  pop(); 

 // Får punkter for ansigt og gemmer det i en variable
 positioner = ctracker.getCurrentPosition(); 
 
 /* Jeg gætter på at den får længden fra ctracker, men dette er jeg i tvivl om.
  Svar gerne i feedbacken, hvis du har et svar */
if(positioner.length){
  
    
// laver et for-loop, gennem alle positionerne, beder den om at starte for enden af arrayet, og slutte når den når den sidste position
for (let i = 0; i < positioner.length; i++) {
  
// tegner ellipse for hvert punkt. 
ellipse(positioner[i][0], positioner[i][1], 5); 

} 
    
}
  

/*Laver her et if-statement så maskinen ved hvad der skal tegnes, 
når der bliver trykket på knappen tilføj filter. */
    if (filter1 === true) {

  //sætter et filter på 
filter(INVERT);
    }


/*Laver her et if-statement så maskinen ved hvad der skal tegnes, 
når der bliver trykket på knappen tilføj tøj. */
    if (brillerhat === true) {

   //Tegner briller
push()
fill(0)
strokeWeight(10);

/* 2D-array, når der står 27 0, betyder det at der er en tabel, 
hvor den har et 0 og et 1, og så tæller den op til 27. 
Det man gør når den siger 27, så tjekker den hvad der står i den 
kasse og derefter tjekker hvad der står i kassen 27 1. 
Hver gang ansigtet bevæger sog, vil værdien ændre sig */
ellipse(positioner[27][0], positioner[27][1], 55, 55);
ellipse(positioner[32][0], positioner[32][1], 55, 55);
line(positioner[27][0], positioner[27][1], positioner[32][0], positioner[32][1]);
pop()

//Tegner hat
push(); 
fill(0); 
rectMode(CENTER); 
square(positioner[33][0], positioner[33][1]-200, 170);
rect(positioner[33][0], positioner[33][1]-100, 230, 55);
pop(); 
 }


/*Laver her et if-statement så maskinen ved hvad der skal tegnes, 
når der bliver trykket på knappen tilføj ansigt. */
 if (mundnose === true) {
 //Tegner næse
push(); 
fill(255, 0, 0); 
ellipse(positioner[62][0], positioner[62][1], 60, 60);
pop(); 

//Tegner læber
// top lip
push();
//rykker øverste læbe til positionen 0, -100
fill(0, 255, 0);
noStroke();

arc(positioner[60][0], positioner[60][1],75,20,PI,0);

// bottom lip
fill(255, 0, 240);
noStroke();
arc(positioner[57][0], positioner[57][1],100,40,0,PI);
pop();

      }
  
    }

















