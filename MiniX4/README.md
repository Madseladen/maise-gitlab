# **MINIX4 - HIDE YOUR IDENTITY**

Her er et screenshot af mit program: 
<img src="Skærmbillede1.png">
<img src="Skærmbillede2.png">
<img src="Skærmbillede3.png">
<img src="Skærmbillede4.png">


Her er et link til mit program: 
[https://maisealling.gitlab.io/maise-gitlab/MiniX4/index.html]


## En titel og en beskrivelse af mit program 
Titlen på mit projekt omkring data capturing er HIDE YOUR IDENTITY. Jeg har valgt denne titel, da jeg synes det omfavner mit projekt godt, og sætter et kitisk blik på det her med at indfange og bruge data. 

Mit program og værk går ud på at man kan se sig selv, med en masse prikker som omkrandser ens ansigt, præcis som man er. I toppen af skærmen står der en neongrøn tekst "Press the buttons and hide your identity". 
I bunden af skærmen ses 3 knapper med teksterne "Add clothes", "Add filter" og "Add face". 
Når man trykker på knappen "Add clothes", så vil ens ansigt få briller og hat på. Når man trykker på knappen "Add fliter", så vil filteret INVERT, blive tilføjet til skærmen. Når man trykker på knappen "Add face", så vil man få en farvet mund og en rød klovnenæse. 

Jeg har i mit program ekperimenteret med dafangst i forhold til webcam, og hvordan man ser ud. 
Når man bruger sit webcam eller uploader billeder til internettet, så er der en masse data der bliver indsamlet omkring, hvordan man ser ud. Ens data bliver sat ind i forskellige algoritmer, og tilføjet i bestemte bokse. Foreksempel ville jeg måske blive sat i en boks der hedder "Hvid ung kvinde, med grønne øjne, og lyst hår, kommer sandsyneligvis fra Europa, mere specifikt norden". 

Når man benytter sig af mit program, vil man altså gennem knapperne "Add clothes", "Add filter" og "Add face", kunne skjule sig selv og sit udseende. Jeg ved at computeren allerede indhenter data af mit ansigt i starten af programmet og sikkert bruger den data, men det har jeg valgt at se bort fra. Jeg har prøvet at udfordre det lidt og sætte fokus på, at jeg ikke vil have at en AI-genereret algoritme skal kunne gøre dette og ved at gemme mig selv eller "Hide My Identity", vil jeg altså gemme min identitet og mit udseende for disse bestemte kasser. 
 

## Mine tanker bag min kode og data capturing, hvordan mit program og mine tankegang adresserer temaet "fang alt". 
Måske har du hør om en app kaldet ImageNet Roulette. Dette er et projekt lavet af Paglen, hvor de har skabt en app som afsløre de skævheder som kan være i indlejret billedgenkendelsessystemer. I denne app, får brugeren mulighed for at uploade et billede af sig selv og giver herefter brugeren en AI-genereret etiket ud, som er baseret på hvordan personen ser ud. https://www.theguardian.com/technology/2019/sep/17/imagenet-roulette-asian-racist-slur-selfie . Mit projekt tager lidt afspejling i dette projekt. Jeg har prøvet at lave et værk, som sætter sig i strid med at en AI-genereret algoritme kan bestemme, hvilken person jeg er, eller hvilken kasse jeg høre til.

Jeg har derfor valgt at benytte mig af webcam og eksperimentere med dette. 

Dataindsamling er med til at oprette og fodre disse kasser med information og viden. Disse kasser er med til at forandre vores verdensforståelse og relations forståelse til hinanden og os selv. 
"Data is the “material produced by abstracting the world into categories, measures and other representational forms [...] that constitute the building blocks from which information and knowledge are created”" (Kitchin, 2014, p. 1). (Datafication, Mejias, 2019).
Som der står i citatet her, er data med til at danne de byggesten, hvorfor information og viden skabes. Data er altså med til at danne bestemte billeder af verden, som den ønsker os til at se den. 

Bevæger vi os tilbage til mit program, prøver jeg at gå i strid med dette, og på en måde forvirre den AI-genererede algoritme. Jeg vil altså ikke være med til at fodre disse kasser med information omkring mit udseende og hvordan en kvinde i norden, måske ser ud. 
Mit værk skal altså være et opråb til vores lyst til fri vilje, og lysten til selv at danne vores tanker, meninger og ideer om hvordan verden ser ud og ikke hvordan en AI-genereret dataindsamlings maskine, har valgt at den skal se ud og præsenteres. 


## Min kode

I starten ses alle min globale variabler, som jeg skal bruge igennem min kode. 
~~~~

/* laver en variable video, 
så programmet ved at det skal bruge webcam*/
    let video; 

// laver en variable for trackeren. 
    let ctracker; 

/* laver en variable til knappen tilføj filter*/
    let knap; 

// Laver en variable til knappen tilføj tøj
    let knaptøj; 

// Laver en variable til knappen tilføj ansigt
    let knapansigt; 

//Laver en global variable for positioner
    let positioner; 

//Laver variabler til mit ansigt, tøj og filter
    let mundnose; 
    let brillerhat; 
    let filter1; 
~~~~
Her laver jeg en knap med teksten Tilføj filter. 
Jeg sætter knappens position, samt størrelsen på knappen. 
Derudover bruger jeg variablerne knap og filter 1, til at sige at når knap bliver trykket på, så vil filter1 bliver aktiveret. Det samme gør jeg for knappen tøj og knappen ansigt. 
~~~~

function setup() {

createCanvas(700, 700); 

/*Opretter kanppen med createButton, dette er et DOM-element*/
    knap = createButton("Tilføj filter"); 

/*Placere knappen på canvas */
    knap.position(300, 600); 

/*Sætter størrelsen på knappen */
    knap.size(100, 50); 

// Gør sådan at når man trykker på knappen tilføj filter, så vil variablen filter1, være sand. 
    knap.mousePressed(() => filter1 = true); 

~~~~
Her får jeg min video, op og køre på mit canvas, så jeg kan se mig selv. Samt tracke mit ansigts positioner. Dette var faktisk det første jeg gjorde i koden. 
~~~~

 /*createCapture er funktionen som vil kreere 
  et capture objekt, som er i video, gør at jeg kan se mig selv*/
    video = createCapture(VIDEO); 

//Sætter størrelsen på videoen
    video.size(700, 700); 

  /* gør at vidoen er på den øverste del af 
  skærmen, ellers ville den have lavet to skærme*/
    video.hide(); 

 // tilføjer min tracker, tildeler variablen tracker, tracker elementet. 
    ctracker = new clm.tracker(); 
    ctracker.init(pModel); 
    ctracker.start(video.elt); 
    }
~~~~
Alt det forgående er skrevet i set-up, da det kun skal køre 1 gang. 

Det næste bliver skrevet i draw da det skal køre i loop. 

Her bruger jeg image, og referer til min video, sætter den til at starte i koordinatet 0,0 med brede og højden 700, 700. 
~~~~

function draw() {

  /* benytter image, for at tage billedet som
  videoen skaber oppe i canvaset som jeg har lavet
  sætter det i koordinatet 0,0, da det skal starte her*/
    image(video, 0, 0, 700, 700); 
~~~~
Her benytter jeg min variable positioner, og skriver at ctracker skal finde currentPosition, til alle punkterne på ansigtet. 

Forstår ikke helt lenght (Se kode note). 

Laver et for-loop, så min maskine ved, at index skal starte forenden 0, og så når index er mindre en positioner length, så skal den ligge 1 til hvert index altså hvert punkt. 

Tegner tilsidst ellipse ved hvert punkt. 
~~~~
 // Får punkter for ansigt og gemmer det i en variable
    positioner = ctracker.getCurrentPosition(); 
 
 /* Jeg gætter på at den får længden fra ctracker, men dette er jeg i tvivl om.Svar gerne i feedbacken, hvis du har et svar */
    if(positioner.length){
  
    
// laver et for-loop, gennem alle positionerne, beder den om at starte for enden af arrayet, og slutte når den når den sidste position
    for (let i = 0; i < positioner.length; i++) {
  
// tegner ellipse for hvert punkt. 
    ellipse(positioner[i][0], positioner[i][1], 5); 
    } 
    }
~~~~
Her laver jeg et if-statement for min evariabler, som fortæller at når filter1 (eksempel) er sandt så skal den tillægge filter INVERT
~~~~

/*Laver her et if-statement så maskinen ved hvad der skal tegnes, 
når der bliver trykket på knappen tilføj filter. */
    if (filter1 === true) {

  //sætter et filter på 
    filter(INVERT);
    }
~~~~
Det samme gør jeg for briller og hat. 
For at finde de præcise koordinater som objekterne skal følge mit ansigt laver jeg et 2D-array: 2D-array, når der står [27] [0] [27] [1], betyder det at der er en tabel, hvor den har en 0 række og en 1 række, og så tæller den op til 27. 
Det den gør når den siger 27 0, så tjekker den hvad der står i den 
kasse og derefter tjekker hvad der står i kassen 27 1. 
Hver gang ansigtet bevæger sig, vil værdien ændre sig
~~~~
/*Laver her et if-statement så maskinen ved hvad der skal tegnes, 
når der bliver trykket på knappen tilføj tøj. */
    if (brillerhat === true) {

//Tegner briller
    push()
    fill(0)
    strokeWeight(10);

/* 2D-array, når der står 27 0, betyder det at der er en tabel, 
hvor den har et 0 og et 1, og så tæller den op til 27. 
Det man gør når den siger 27, så tjekker den hvad der står i den 
kasse og derefter tjekker hvad der står i kassen 27 1. 
Hver gang ansigtet bevæger sog, vil værdien ændre sig */
    ellipse(positioner[27][0], positioner[27][1], 55, 55);
    ellipse(positioner[32][0], positioner[32][1], 55, 55);
    line(positioner[27][0], positioner[27][1], positioner[32][0],positioner[32][1]);
    pop()

~~~~
# Hvad kan gøres bedre
Jeg ville gerne have haft at man kunne tilføje og fjerne objerkterne og filteret fra ens ansigt og slev lave kombinationer, men dette vidste jeg ikke hvordan jeg skulle gøre og jeg have på dette tidspunkt brugt meget tid på min MiniX og magtede ikke mere. Jeg fik knapperne til at skifte farve når man trykekde på dem og skfite tilbage når man trykkede igen, men da objekterne og filteret også skulle komme frem når man trykkede på knapperne, lavede det kuks i min kode. Jeg tror ikke at mousePressed kunne have to ting i sig på samme tid, men ved ikke lige. 
