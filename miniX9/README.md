# FAKE NEWS GENERATOR - MiniX9
**Gruppe 5 - Maise, Mads, Job og Amalie**

Link til program: https://maisealling.gitlab.io/maise-gitlab/miniX9/index.html
Billeder af program: <img src="MiniX9_-_1.png">
<img src="MiniX9_-_2.png">

I vores MiniX9 har vi valgt at lave et værk, som generer Fake News, ved at sætte random sætninger sammen. Vores værk har vi kaldt for ‘Fake News Generator’ og skal være en kontrast til den opfattelse vi har af nyheder og informationer vi optager fra det sociale medie, Facebook. Vores værk skal sætte fokus på kildekritik og hvor svært det er for brugeren at skelne mellem sande og falske news vi ser på Facebook. Denne form for information kan føre til miskommunikation blandt brugeren og dermed skabe misvisende fakta. Vi har valgt at sætte fokus på politik, da dette emne har en stor betydning på de sociale medier, da mange politikere vælger at kommunikere med deres vælgere gennem blandt andet Facebook. Vores fokus på Facebook som socialt medie bunder i at de har en bred målgruppe, som gør at et politisk budskab vil nå langt ud til en stor del af den danske befolkning. Denne metode kan få vælgerne til at ændre deres politiske standpunkt, da de bliver påvirket af de holdninger og meninger, der bliver sendt ud på Facebook. Vi ser derfor en tendens til at der bliver skabt en skæv magtbalance i samfundet og Facebook dermed kan agere som en politisk stormagt og i sidste ende påvirke vælgernes stemme.


Vi har valgt 5 forskellige kilder, som hver især repræsenterer forskellige befolkningsgrupper i samfundet, herunder Ekstra Bladet, Farvskov Kommune, Politiet, Politikken og Peter Falktoft. Ydermere har vi sammensat sætninger delt op i tre kategorier og derefter bliver sammensat random og danner nye sætninger. Disse sætninger understreger vores pointe med Fake News, at alle i princippet kan tilføje og skabe Fake News, hvis bare de har den rigtige platform til dette.


**Beskriv, hvordan dit program fungerer, og hvilken syntaks du har brugt og lært?**


I denne MiniX9 har vi gjort brug af JSON-filen hvor vi sammensætter vores sætninger. I en JSON-fil kan man opsætte tekst i arrays og dermed skabe en dynamisk kode. En JSON-fil fungerer ligesom en class hvor du  fylder dets indhold, som man dermed kan referere til i sin sketch.js fil. I vores JSON fil har vi delt vores sætninger op i fire value pairs: intro, kryds, cirkel og afslutning. I hver kategori har vi valgt et tema der repræsenterer en handling eller en form for information. Vores ‘intro’ value pair beskriver et form for opråb, som fanger brugerens opmærksomhed, og er separeret med kolon samt hvert item(opråb) separeres med komma _(referere til linje 2-7 i JSON-fil)_. Denne form for opsætning har vi brugt ved hver kategori.


```
{
   "intro": [
       "Vidste du at",
       "Har du hørt det?!",
       "NYHED NYHED NYHED NYHED",
       "UHØRT!",
       "Nu sker der fanme noget!"
   ],
```


I vores sketch-fil starter vi med at kalde på vores JSON-fil ved at oprette en global variabel.


`let myJson;`


For at loade vores JSON-fil, indsætter vi funktionen `‘function preload’` og kalder vores JSON-fil ved at skrive navnet på vores JSON fil.


```
function preload() {
   myJson = loadJSON("jam.json");
```




I denne funktion indhenter vi også vores fem billeder, som vi har uploadet til vores mappe.


```
  img1 = loadImage('eb.png');
   img2 = loadImage('fk.png');
   img3 = loadImage('p.png');
   img4 = loadImage('pf.png');
   img5 = loadImage('pt.png');
}
```


For at placeres vores tekst det rigtige sted på vores canvas skal vi bruge enkelte globale variabler som hver især forskyder teksten langs `x- og y-aksen` samt bestemmer afstanden mellem hver individuel linje i teksten.


```
let h = 30


let sandheden = 60 //x-aksen


let systematiskeLøgne = 160
```


I vores `setup funktion` opretter vi et canvas der placeres så den passer til skærmens `width og height`. For at skabe den mest naturlige implementering af tekst, der passer til den font-stil som Facebook benytter, har vi valgt at bruge `tekst font typen ‘Geneva’`.


```
textFont('Geneva')


fill(75);
```


For at tilføje vores billeder så de passer til vores canvas, så har vi indsat dem i `‘function draw’`, så de looper igen og igen.


Vi benytter her syntaksen `img1.resize(width, height`) for at tilpasse størrelsen på billedet til en ny bredde og højde. Her har vi valgt at indsætte `width` og `height` så den passer til canvas. Dette gentages ved alle fem billeder.


```
img1.resize(width, height)
   img2.resize(width, height)
   img3.resize(width, height)
   img4.resize(width, height)
   img5.resize(width, height)


```


I vores `function setup` har vi sat vores `frame rate` til at være `0.3` som er den hastighed, billederne skifter i.


`frameRate(0.3)`


Dernæst tilføjer vi en lokal variabel vi kalder `let r,` som vi sætter til at være `ceil(random(5))`. Her bruger vi `ceiling`, så vi kun får hele tal ud af det, samt bruger random funktionen som gør at vi skifter baggrundsbilledet tilfældigt sammen med nedenstående `if-else statements`.


` let r = ceil(random(5))`


I vores `if-else statements` fortæller vi programmet at den skal vælge random mellem vores 5 billeder, det vil sige vælge mellem `1-5 random`.


```
 if (r == 1) {
       image(img1, 0, 0);
   } if (r == 2) {
       image(img2, 0, 0);
   } if (r == 3) {
       image(img3, 0, 0);
   } if (r == 4) {
       image(img4, 0, 0);
   } if (r == 5) {
       image(img5, 0, 0);
   }
```
Til sidst tilgår vi JSON-filen og indsætter teksten samt tilføjer `floor(random)` som bestemmer, hvad der skal stå på hver linje. Vi bruger `floor(random)` funktionen da den beregner den nærmeste int-værdi der er mindre end eller lig med parameterens værdi.




```
text(myJson.intro[floor(random(5))], width / 2 - sandheden, height / 2 + systematiskeLøgne);
   text(myJson.kryds[floor(random(9))], width / 2 - sandheden, height / 2 + systematiskeLøgne + h);
   text(myJson.cirkel[floor(random(30))], width / 2 - sandheden, height / 2 + h * 2 + systematiskeLøgne);
   text(myJson.afslutning[floor(random(12))], width / 2 - sandheden, height / 2 + h * 3 + systematiskeLøgne);
}
```
## Vocable Code


“Using the phase Vocable Code for this chapters title aims to make explicit how the act of coding cannot simply be reducible to its functional aspects. Rather we would like to emphasize that code mirrors that instability inherent in human language in terms of how it expresses itself, and is interpreted..” _(Soon, Cox, 2020 s. 167)_


Når man tænker på kode, vil man højst sandsynligt forestille sig at der er en masse tal, 0 og 1 taller, sætninger som ikke giver mening men, som gør at der sker noget. Men selve koden kan også være noget i sig selv, som ikke har behov for en krop eller noget visuelt. Teksten i koden kan derfor stå for sig selv og give mening uden at man kan se det reelle output. Når vi forestiller os kode er det meget konkret, sort og hvidt og resultatorienteret, men ligesom i den virkelig verden findes der flere aspekter, nuancer af grå og koden kan på den måde i vocable code læner sig tættere op af menneskesprog og den virkelige verden. Koden kan i den forbindelse være flere ting på en gang; input der giver et output, poesi stykke, en historie eller noget helt tredje, alt efter hvad programmøren vil udtrykke.


Når vi navngiver vores variabler, er det ligegyldigt for programmet og selve eksekveringen af programmet om vi kalder vores variabler for x, offset , billede4 eller noget politisk som f.eks. What Is Queer. Den eneste tekniske forskel er at flere tegn vil forøge filstørrelsen med få kilobyte, men for programmøren og mennesket bagved kan det have betydning. Ud over at det er mere overskueligt at navngive logisk, kan det også være en måde for programmøren at udtrykke sig selv. Det sker i forskellig grad fra program til program, men i eksemplet “Vocable code” ser vi et eksempel på hvordan koden i høj grad også er en del af budskabet og her udtrykket programmøren sig tydeligt gennem selve koden, som Soon & Cox skriver det: “This is where we hear the programmer’s voice.”_(Soon & Cox, 2020)_


p5.js er et high-level programming sprog, hvilket kan give brugeren mere kontrol og meget mere brugervenlighed, men det øger også skellet mellem den kode  programmøren skriver og de processer der sker inde i computeren. Denne process bliver kaldt “magi” af Wendy Hui Kyong Chun, og i denne oversættelse fra high-level programming sprog til binær er et biprodukt at der sker en højere grad af blackboxing -  programmøren ved ikke præcist hvad der sker i oversættelsen, men kan blot se sin egen kode og slutresultated._(Soon & Cox, 2020)_


Ifølge Soon og Cox (2020) skal source code og det program som det eksekverer ikke ses som identiske men hellere som en oversættelse, og dette stiller også spørgsmålstegn ved interface-princippet “what-you-see-is-what-you-get” - for kan det overhoved lade sig gøre?




## Reference liste


https://p5js.org/reference/


Soon, Winnie & Cox, Geoff - Aesthetic programming, A handbook of Software Studies


