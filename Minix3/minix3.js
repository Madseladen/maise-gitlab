// laver variable for speed
let speed = 1; 
// variable for y
let y = 750; 
// laver variable for pølens brede
let b = 50; 
// laver hastighed for pøl
let speedpool = 1; 

// Laver x placeringen til en variable, som vi kan bruge senere i koden. 
let regndropplaceringX; 
// laver y placeringen til en variable, og sætter dne til 700, så kan vi bruge den senere i koden. 
let regndropplaceringY = 700; 


//throbber
function setup() {
    //create a drawing canvas
    createCanvas(windowWidth, windowHeight);
    noStroke(); 
    
    // definere regndråbens placering på X-aksen
    regndropplaceringX = windowWidth/2; 
 
  }
  

  
   function draw() {
    background(102, 102, 102); 
// laver et for-loop
    for(let i = 0; i <= 100; i++) {

// gør at dråberne falder random mellem værdierne - 100 og 100
   makedrop(regndropplaceringX + random(-250, 100), regndropplaceringY + random(-100, 100));   
      }
 //* tegner sky
    fill(204, 204, 204); 
    noStroke(); 
    ellipse(windowWidth/2+70, windowHeight/2-200,300); // diameter her er 300
    ellipse(windowWidth/2-70, windowHeight/2+1, 400); 
    ellipse(windowWidth/2+140, windowHeight/2+1, 400);  
    ellipse(windowWidth/2-300, windowHeight/2+1, 400); 
    ellipse(windowWidth/2-200, windowHeight/2-250,350); 
    

    // tegner pøl og får pøl til at bevæge sig, med speeden 1. 
    if (b <= 600) {
    speedpool = 1; 
    }
    if(b > 600){
      background(102); 
      b = 50; // breden af pølen stopper her. 
    }
    //tegner pøl og sætter b + b = speedpool som gør at pølen bevæger soig med den definerede speed. 
    fill(153, 255, 255); 
    ellipse(windowWidth/2.1, windowHeight/2+400, b, 50); 
    b = b + speedpool; 
    

  
  
  

  //laver speed hastighed og placering 
  //når y er mindre end 900, skal den vokse med 1, ellers så skal y går tilbage til 700. 
  if(regndropplaceringY < 830) {
  regndropplaceringY++; 
  }
  else {
    regndropplaceringY = 700; 
  }

// tegner dråben og laver den til en function som jeg kan bruge videre i min kode. 
function makedrop(x,y) {
  noStroke(); 
  fill(153, 255, 255); 
  ellipse(x, y, 20); 
  }
}




