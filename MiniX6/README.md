## MINIX6

Her er et link til mit program: 
https://maisealling.gitlab.io/maise-gitlab/MiniX6/index.html

Her er billeder af mit program: 
<img src="MIniX61.png">
<img src="MiniX62.png">
<img src="MiniX6.png">

## Bskrivelse af mit spil og spilobjekter. 
(Mit spil virker ikke optimalt, men jeg vil forbedre det til MiniX7. Det der ikek fungere er distancen, mellem spiller og flue, så selve essensen af spillet virker ikke, da man ikke akn fange fluerne og få point. Du kan pt. kun tabe spillet) 
Mit spil går ud på, at man skal klasske/fange fluerne med hånden. Fluerne vil poppe op på skærmen random, og så går det altså ud på at styre hånden med musen, hen over fluerne, og få så mange point man kan. Hvis man har misset 10 fluer inden for 1 minut, så vil man tabe spillet. 

## Min kode
Fluen:  Jeg har lavet en klasse til min flue, hvor jeg har implementeret en constructor, med x og y positioner samt tid, som er fameCount, denne constructer er med til at gøre at man kan tabe spillet. 

Jeg har nede i show(), tegnet fluen og implementeret mine x og y positioner som er defineret oppe i constructeren. 
~~~~
class Flue {
    constructor() { // øversætter noget som classen kan forstå. 
        this.posX = random(width); // Gør så de bliver placeret random på skærmen
        this.posY = random(height);
        this.tid = frameCount;  // siger at fluens tid = frameCount
    }

    show() { // tegner fluen
        // tegner krop
        fill(150, 100, 50);
        ellipse(this.posX-30, this.posY-5, 50, 25);

        // tegner hoved
        fill(50, 100, 150);
        ellipse(this.posX-5, this.posY-5, 15, 15);

        // tegner øjne
        fill(255);
        ellipse(this.posX, this.posY-10, 5, 5);
        ellipse(this.posX, this.posY, 5, 5);

        // tegner vinger
        fill(255, 200);
        ellipse(this.posX-30, this.posY-15, 35, 15);
        ellipse(this.posX-30, this.posY+5, 35, 15);
    }
}
~~~~
Spiller: Det samme har jeg gjort for mit spiller objekt. 
~~~~
class Spiller {
    constructor(){

//Sætter mine start positioner for x og y, 
//når jeg sætter min this.posX og Y, til at være mouseX og mouseY, og referere til dem nede i show, så virker det ikke. Derfor er det skrevet  på denne måde. 
    this.posX = mouseX; 
    this.posY = mouseY;
        
    }
    
    show(){
 // Tegner håndfladen
    fill(255, 200, 150);

// x og y, skal følge musen og bliver derfor skrevet som mouseX og mouseY
    ellipse(mouseX, mouseY, 100, 120);
  
// tegner fingrene
    fill(255, 200, 150);
    ellipse(mouseX-20, mouseY-60, 30, 90);
    ellipse(mouseX+10, mouseY-70, 30, 90);
    ellipse(mouseX+40, mouseY-40, 30, 80);
  
  
// Tegner tommeltotten 
    fill(255, 200, 150);
    ellipse(mouseX-45,mouseY-20, 30, 60);
  
// Tegner neglene
    fill(255);
    ellipse(mouseX-20, mouseY-90, 20, 20);
    ellipse(mouseX+10, mouseY-100, 20, 20);
    ellipse(mouseX+40, mouseY-65, 20, 20);
    ellipse(mouseX-45, mouseY-40, 15, 15);

    }
    }
~~~~
Spillet: 

Jeg har startet med at lave globale variabler, og sat min flue ind i et array, da der skal være flere end en flue. 
~~~~
//Laver globale variabler som jeg kan bruge senere i min kode

// laver et array, som skal bruges til for-loop. 
    let flue = [];
    let spiller;
    let antal = 2;
    let point = 0;
    let lose = 0; 
    let tid = 0;
~~~~
Her i function setup, gør jeg at min spiller bliver vist. Jeg laver også et for-loop, som gør at der vil være flere flue (i alt to, da antal er sat til 2 oppe i mine globale variabler). flue.push(new Flue()); gør at fluen bliver taget fra klassen og tegnet på skærmen. 
~~~~
    function setup() {
    createCanvas(windowWidth, windowHeight);
    spiller = new Spiller();

//forloop som laver så mange antal fluer, jeg ønsker og pusher 2 fluer ind i arrayet. Denne bliver sat i setup, 
//da den kun skal køre 1 gang, ellers ville den blive ved med at generere flere fluer. 
    for (let i = 0; i < antal; i++) {
    flue.push(new Flue());
    }
    }
~~~~
I draw, kalder jeg på mine funktioner, som jeg har lavet længere nede, for at de bliver tagenet og kørt i loop, på min skærm. 
~~~~
function draw() {
// tegner baggrunden som jeg har preloadet. 
    background(baggrund);

// her bruger jeg min variable, og ligger 1 til tid, efter hver frame der er 60 frames 
//pr. sekund. Denne kan jeg bruge til mit lose-statement. 
    tid++

// den tegner fluen på canvaset og kalder på funktionen
    showFlue();
    spiller.show();
    ramtFlue();
    pointDisplay();
//checkResult();

// laver et if-statement som siger, at hvis du ikke har fået 10 point inden for 1 minut, så vil du tabe. Denne er ikke helt korrekt, da dne egentlig siger, at når der er gået 1 minut, så har du tabt.
    if (tid == 1000) {
    fill(0);
    textSize(40);
    text("Too many flies in your food...GAME OVER", width/2, height/2);
  
//Dette skal stoppe spillet og derfor ikke gå i loop. 
    noLoop();
      }

}
~~~~
Her laver jeg en funktion, hvor jeg bruger min metode show(), fra min flue klasse. 
~~~~
// laver en funktion som, som laver et for-loop, der tegner antallet af fluer. 
    function showFlue() {

//her bruger jeg arrayet, som jeg har sagt er 2, så viser altså de to objekter
    for (let i = 0; i < flue.length; i++) {

// tegner selve fluen, og tager en ny flue fra arrayet og viser. 
    flue[i].show();

/* laver et if-statement som sige, at hvis framCounten er større
end flue[1].tid +90, så skal den splice fluen og lave nye. vi har i vores flue Class, sat 
this.tid, som er den vi referere til at være frameCount. [i], referere til vores let flue [], som er det
antal fluer den skal tegner nemlig i alt 2, den referere altså til det for-loop, som vi har lavet oppe i 
setup. + 90, vil sige at når fluen har været på skærmen i mere end frameCount + 90, så skal den forsvinde. 
flue.splice (i, 1); er den der gør at fluen forsvinder, og flue.push(new Flue()); er den der gør at der bliver
lavet nye fluer. */
    if (frameCount > flue[i].tid + 90) {
    flue.splice(i, 1);
    flue.push(new Flue());
    }
    }

    }
~~~~
Her laver jeg en funktion, for ramt fluer, som skal gøre at når spiller og flue er på samme x og y position så skal man få et point. Dette virker dog ikke, men jeg vil prøve at optimere det i min næste MiniX. 
~~~~
function ramtFlue() {
    let distance; 
    for (let i = 0; i < flue.length; i++) {
        distance = dist(spiller.posX, spiller.posY, flue[i].posX, flue[i].posY); 

    if (distance < spiller.posX) {
       flue.splice(i,1); 
       flue.push(new Flue());
        point++; 
    }
    }
}
~~~~
Her laver jeg en funktion for min point tæller. 
~~~~
// laver en funktion hvor jeg tegner point, og placere den på canvas
    function pointDisplay() {
    fill(0);
    textSize(40);
    text('Point;' + point, width / 2, 50);
   
    }
~~~~

## Beskriv, hvordan du programmerer objekterne og deres relaterede attributter, og metoderne i dit spil.
Jeg programmere mine objekter, ved at lave dem inde i en class for sig altså i deres egen js. Inde i den der laver jeg en constructer, hvor jeg definere min x og y position som eksempel, samt tid. Disse kan jeg så bruge i min metode nemlig show(), og inde i mit spil til at referere til bestemte koordinater. 
I show(), som er min metode, der programmere jeg hvordan mine objekter skal se ud. Her tildeler jeg dem forskellige attributter, som for eksempel, hvilken farve eller form de skal have. 
Disse attributter, som er defineret i constructer og metode kan jeg så referere til og bruge i min observærtør, nemlig mit spil. 

## Hvad er kendetegnene ved objektorienteret programmering og de bredere implikationer af abstraktion? 
"In programming an object is a key concept, but it is also more generally understood as a thing with properties that can be identified in relation to the term subject." (Soon & Cox, 2020). 
Inden for programmering vil man sige at objekt er et nøglebegreb, det er en ting med egenskaber, som kan identificeres i forhold til emnet. Et emne er en opservatør (vores endelige spil, kan man sige). Et objekt er en ting som vi designer inde i vores klasse, som vores spil (i dette tilfælde) observere og bruger. 
I objektorienteret programmerign arbejder man med at konstruere objekter, attributter og adfær som skal repræsentere aspekter af den virkelige verden. Abstraktionsprocessen i dette er at de fysiske objekter skal oversættes til ideen om et objekt i programmering. 

Abstraktion er et paradigme inden for programmering, det vil sige at programmer er organiseret omkring data eller objekter snarere end funktioner og logik. Det går ud på af håndtere objektets kompleksitet ved at abstrahere visse detaljer så som farve og form, og præsentere en konkret model af dette objekt. 

"Beatrice Fazi and Matthew Fuller have outlined the wider significance of this and the relations between concrete and abstracted computation: “Computation not only abstracts from the world in order to model and represent it; through such abstractions, it also partakes in it.” 2 If we recall the previous chapters and the many examples of data capture, and gamification, it becomes clear that computation can shape certain behaviors and actions. In other words, objects in OOP are not only about negotiating with the real world as a form of realism and representation, nor about the functions and logic that compose the objects, but the wider relations and “interactions between and with the computational."" (Soon & Cox, 2020). 

"Discussing the development of Smalltalk, Kay says: ‘object-oriented design is a successful attempt to qualitatively improve the efficiency of modelling the ever more complex dynamic systems and user relationships made possible by the silicon explosion’ " (Fuller & Goffey, ??)

Objektorienteret programmering handler ikke kun om at forme disse objekter så de ligner verden mest muligt, men også at skabe de relationer som mennesker har til objekterne og hvordan de inteargere med objekterne i den virkelig verden. 

"Crutzen and Kotkamp argue that OOP is based on “illusions of objectivity and neutrality of representation,” in which “[a]bstractions are simplified descriptions with a limited number of accepted properties. They reply on the suppression of a lot of other aspects of the world.”" (Soon & Cox, 2020). 

Det der menes her er at obejkter i den virkelige verden er meget kopmplekse. Abstraktions- oversættelsesprocesser indebære beslutningstagning og prioritisering af gennerelle aspekter af objektet, som kan forstås og bruges i programmeringen. Man kan altså sige at vi forenkler objektet fra den virkelige verden for at kunne repræsentere den i computerverdenen. 

"It’s also worth reiterating that OOP is designed to reflect the way the world is organized and imagined, at least from the computer programmers’ perspective. It provides an understanding of the ways in which relatively independent objects operate through their relation to other objects." (Soon & Cox, 2020). 

## Mit spils virkelige kontekst. 
Mit spil går som sagt ud på at klaske fluer. Vi kender alle det med at man sidder en varm sommeraften og spiser ude i haven. Men så kommer der vipse og bier og fluer, og sætter sig i alt den lækre mad. Det er bare træls. Her har jeg valgt at tage fluen, som objekt som vi kender den fra den virkelig verden. I min class, har jeg udvalgt de mest generelle abstraktioner som fluen har, nemlig vinger, øjne, og krop. Jeg har dog gjort fluen en anelse større end normalt, så man bedre kunne se den i spillet. Der ud over har jeg taget hånden som mit andet objekt. Da det der sker når en flue kommer og sætter sig i ens mad, så prøver man at vifte den væk eller klaske den med hånden. Jeg har også her taget de mest genereller abstraktioner fra en hånd, nemlig fingre, negle og håndryg. Jeg har dog fjernet en finger, da jeg vurderede at det så for under ligt ud. den sidste finger ville sidde en anelse akavet ude på siden, og jeg mener at man forstår ideen af at det er en hånd. 
Relationen mellem hånden og fluen er at fluen bliver klasket eller viftet væk, med hånden. Det er dette der giver point i mit spil (det virker ikke). Her har jeg altså implementeret den virkelige relation og forenklet den lidt i mit spil. 
