
//Laver globale variabler som jeg kan bruge senere i min kode

// laver et array, som skal bruges til for-loop. 
let flue = [];
let spiller;
let antal = 2;
let point = 0;
let lose = 0;
let tid = 0;



// sætter baggrunden til at være dette billede. Samt preloader mine to giffer. 
function preload() {
    baggrund = loadImage("tabel.baggrund.avif");
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    spiller = new Spiller();

    //forloop som laver så mange antal fluer, jeg ønsker og pusher 2 fluer ind i arrayet. Denne bliver sat i setup, 
    //da den kun skal køre 1 gang, elelrs ville den blive ved med at generere flere fluer. 
    for (let i = 0; i < antal; i++) {
        flue.push(new Flue());
    }
}

function draw() {
    // tegner baggrunden som jeg har preloadet. 
    background(baggrund);

    // her bruger jeg min variable, og ligger 1 til tid, efter hver frame der er 60 frames 
    //pr. sekund. Denne kan jeg bruge til mit lose-statement. 
    tid++

    // den tegner fluen på canvaset og kalder på funktionen
    showFlue();
    spiller.show();
    ramtFlue();
    pointDisplay();
    

    // laver et if-statement som siger, at hvis du ikke har fået 10 point inden for 5 millisekunder, så vil du tabe. Dette har jeg gjort flere gange for at man kan nå længere i spillet. 
    if (tid == 500 && point < 10) {
        fill(0);
        textAlign(CENTER); 
        textSize(50);
        text("Too many flies in your food...GAME OVER", width / 2, height / 2);
        
        //Bruger min funktion og siger at den skal starte med den første sværhedgrad. 
        final(1); 

        // Dette skal stoppe spillet og derfor ikke gå i loop. 
        noLoop();
    } else if (tid == 700 && point < 20) {
        fill(0);
        textAlign(CENTER); 
        textSize(50);
        text("Too many flies in your food...GAME OVER", width / 2, height / 2);
        final(2); 

        // Dette skal stoppe spillet og derfor ikke gå i loop. 
        noLoop();
    } else if (tid == 900 && point < 30) {
        fill(0);
        textAlign(CENTER); 
        textSize(50);
        text("Too many flies in your food...GAME OVER", width / 2, height / 2);
        final(3);

        // Dette skal stoppe spillet og derfor ikke gå i loop. 
        noLoop();
    } else if (tid == 1100 && point < 40) {
        fill(0);
        textAlign(CENTER); 
        textSize(50);
        text("Too many flies in your food...GAME OVER", width / 2, height / 2);
        final(4); 

        // Dette skal stoppe spillet og derfor ikke gå i loop. 
        noLoop();
    } else if (tid == 1300 && point < 50) {
        fill(0);
        textAlign(CENTER); 
        textSize(50);
        text("Too many flies in your food...GAME OVER", width / 2, height / 2);
        final(5); 

        // Dette skal stoppe spillet og derfor ikke gå i loop. 
        noLoop();
    } else if (tid == 1500 && point < 60) {
        fill(0);
        textAlign(CENTER); 
        textSize(50);
        text("Too many flies in your food...GAME OVER", width / 2, height / 2);
        final(6); 

        // Dette skal stoppe spillet og derfor ikke gå i loop. 
        noLoop();
    }

}

// laver en funktion som, som laver et for-loop, der tegner antallet af fluer. 
function showFlue() {

    // her bruger jeg arrayet, som jeg har sagt er 2, så viser altså de to objekter
    for (let i = 0; i < flue.length; i++) {

        // tegner selve fluen, og tager en ny flue fra arrayet og viser. 
        flue[i].show();

        /* laver et if-statement som sige, at hvsi framCounten er større
            end flue[1].tid +40, så skal den splice fluen og lave nye. vi har i vores flue Class, sat 
            this.tid, som er den vi referere til at være frameCount. [i], referere til vores let flue [], som er det
            antal fluer den skal tegner nemlig i alt 2, den referere altså til det for-loop, som vi har lavet oppe i 
            setup. + 80, vil sige at når fluen har været på skærmen i mere end frameCount + 40, så skal den forsvinde. 
            flue.splice (i, 1); er den der gør at fluen forsvinder, og flue.push(new Flue()); er den der gør at der bliver
            lavet nye fluer. */
        if (frameCount > flue[i].tid + 90) {
            flue.splice(i, 1);
            flue.push(new Flue());
        }
    }

}



function ramtFlue() {
    // laver her et for-loop, som siger at det der er inde i for-loopet skal gælde for alle objekter (flue) inde i arrayet. 
    //for-loopet tæller fra 0 - 2 8med 2 tæller ikke med), og så bliver der hele tiden lagt en til. Første gange den looper, lser den som flue som flue index og anden gang læser den som flue index 1. 
    for (let i = 0; i < flue.length; i++) {


        //Hvis mseuns x position er større end fluens position - 10 (en afstand), så splice en flue med 1, og push en ny flue, og så point gå op. 
        if (mouseX > flue[i].posX - 10 && mouseX < flue[i].posX + 10) {
            flue.splice(i, 1);
            flue.push(new Flue());
            point++;
        }
    }
}


// laver en funktion hvor jeg tegner point, og placere den på canvas
function pointDisplay() {
    fill(0);
    textAlign(CENTER); 
    textSize(40);
    text('Point;' + point, width / 2, 50);

}

// Her laver jeg en funktion til min final score. Hvor jeg skriver at hvis du har tabt. 
//Den laver (sværhedgrad) til en variable i min funktion, som jeg bruger til at sige at når 10 gange sværhedsgrad 
//(bruger 10 fordi jeg øger min sværhedgrad med 10 hver gang), så skal den minusse point, så man kan se hvor mange man har tabt. 
//final(tal) er argumentet, (sværhedgrad) er parameter. 
function final(sværhedsgrad) {
fill(0);
textAlign(CENTER); 
textSize(40);
//Her siger jeg at 
text('Flies you missed ' + (10*sværhedsgrad - point), width / 2, 100); 
}

