class Spiller {
    constructor(){

        //Sætter mine start positioner for x og y, 
        //når jeg sætter min this.posX og Y, til at være mouseX og mouseY, og referere til dem nede i show, så virker det ikke. Derfor er det skrevt på denne måde. Så width/2 og height/2 er start positionerne for hånden
        this.posX = mouseX; 
        this.posY = mouseY;
        
    }
    
    show(){
    // Tegner håndfladen
  fill(255, 200, 150);

  // x og y, skal følge musen og bliver derfor skrevet som mouseX og mouseY
  ellipse(mouseX, mouseY, 100, 120);
  
  // tegner fingrene
  fill(255, 200, 150);
  ellipse(mouseX-20, mouseY-60, 30, 90);
  ellipse(mouseX+10, mouseY-70, 30, 90);
  ellipse(mouseX+40, mouseY-40, 30, 80);
  
  
  // Tegner tommeltotten 
  fill(255, 200, 150);
  ellipse(mouseX-45,mouseY-20, 30, 60);
  
  // Tegner neglene
  fill(255);
  ellipse(mouseX-20, mouseY-90, 20, 20);
  ellipse(mouseX+10, mouseY-100, 20, 20);
  ellipse(mouseX+40, mouseY-65, 20, 20);
  ellipse(mouseX-45, mouseY-40, 15, 15);

    }
}

